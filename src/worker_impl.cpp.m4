#include <mutex>
#include <zlib/zlib.h>
//
#include <lux_shared/noise.hpp>
//
#include <worker.hpp>
#include <worker_impl.hpp>
#include <db.hpp>

namespace worker {

static void load_chunk(Uninit<TaskOutput>& out, TaskInput& in);
static void build_chunk_mesh(Uninit<TaskOutput>& out, TaskInput& in);
static void unload_chunk(Uninit<TaskOutput>& out, TaskInput& in);

static void generate_height_map(Uninit<TaskOutput>& out,
                                TaskInput& in) {
    using namespace map;
    auto const& pos  = in.generate_height_map->pos;
    out.init(TaskOutput::GENERATE_HEIGHT_MAP);
    //this cast is a bit suspicious, but it's needed apparently
    out->generate_height_map->h_map = (map::HeightMap*)(new HeightMap);
    HeightMap& h_map = *out->generate_height_map->h_map;
    constexpr Uns octaves    = 16;
    constexpr F32 base_scale = 0.0005f;
    constexpr F32 h_exp      = 3.f;
    constexpr F32 max_h      = 1024.f;
    constexpr F32 turbulence = 0.15f;
    {   Vec2F base_seed = (Vec2F)(pos * (ChkCoord)CHK_SIZE) * base_scale;
        Vec2F seed = base_seed;
        Uns idx = 0;
        for(Uns y = 0; y < CHK_SIZE + 1; ++y) {
            for(Uns x = 0; x < CHK_SIZE + 1; ++x) {
                h_map[idx] =
                    pow(u_norm(noise_fbm(seed, octaves)), h_exp) * max_h +
                    lux_randf(seed) * turbulence;
                seed.x += base_scale;
                ++idx;
            }
            seed.x  = base_seed.x;
            seed.y += base_scale;
        }
    }
}
static void generate_chunk(TaskOutput::LoadChunk& out, TaskInput::LoadChunk& in) {
    using namespace map;

    auto const& pos  = in.pos;
    HeightMap h_map;

    constexpr Uns octaves    = 16;
    constexpr F32 base_scale = 0.0005f;
    constexpr F32 h_exp      = 3.f;
    constexpr F32 max_h      = 512.f;
    constexpr F32 turbulence = 0.15f;
    {   Vec2F base_seed = (Vec2F)(pos * (ChkCoord)CHK_SIZE);
        base_seed -= 1.f;
        base_seed *= base_scale;
        Vec2F seed = base_seed;
        for(Int y = -1; y < (Int)(CHK_SIZE + 1); ++y) {
            for(Int x = -1; x < (Int)(CHK_SIZE + 1); ++x) {
                float sum = 0;
                float freq = 1.0, amp = 1.0;
                Vec2F dsum = Vec2F(0,0);
                for(int i=0; i < octaves; i++)
                {
                    Vec2F s = {lux_randf(i, 0), lux_randf(i, 1)};
                    s -= 0.5f;
                    s *= 2.f;
                    s *= 100.f;
                    Vec3F n = noise_deriv(s + (seed + 0.1f * dsum)*freq);
                    sum += amp * (1.f - abs(n.x));
                    dsum += amp * Vec2F(n.y, n.z) * -n.x;
                    freq *= 2.f;
                    amp *= 0.5f * clamp(sum, 0.f, 1.f);
                }
                sum /= 2.f;
                sum /= (1.f - 2.f * amp);
                Uns idx = (x + 1) + (y + 1) * (CHK_SIZE + 2);
                h_map[idx] = pow(u_norm(sum), h_exp) * max_h +
                    lux_randf(seed) * turbulence;
                seed.x += base_scale;
            }
            seed.x  = base_seed.x;
            seed.y += base_scale;
        }
    }

    out.data = new Chunk::Data;
    auto& chunk         = out.data;
    auto& block_changes = out.block_changes;
    auto get_block =
    [&](ChkIdx const& idx) -> Block& {
        return chunk->blocks[idx];
    };
    auto write_suspended_block =
    [&](MapPos const& map_pos, Block const& block) {
        ChkPos chk_pos = to_chk_pos(map_pos);
        ChkIdx idx = to_chk_idx(map_pos);
        if(chk_pos == pos) {
            get_block(idx) = block;
        } else {
            block_changes[chk_pos].push({idx, block});
        }
    };
    static const BlockId raw_stone  = db_block_id("raw_stone"_l);
    static const BlockId stone_wall = db_block_id("stone_wall"_l);
    static const BlockId dark_grass = db_block_id("dark_grass"_l);
    static const BlockId dirt       = db_block_id("dirt"_l);
    static const BlockId snow       = db_block_id("snow"_l);
    static const BlockId grass      = db_block_id("grass"_l);
#if 0
    for(Uns i = 0; i < CHK_VOL; ++i) {
        MapPos map_pos = to_map_pos(pos, i);

        if(map_pos.z <= 0) {
            chunk->blocks[i].id  = dark_grass;
        } else if(!(pos.x % 2) && !(pos.y % 2) && pos.z == 0) {
            F32 len = distance((Vec3F)map_pos, (Vec3F)to_map_pos(pos, 0) + Vec3F(CHK_SIZE / 2));
            chunk->blocks[i].id = raw_stone;
        } else {
            chunk->blocks[i].id = void_block;
        }
    }
#else
    for(Uns i = 0; i < CHK_VOL; ++i) {
        MapPos map_pos = to_map_pos(pos, i);
        Vec2<MapCoord> h_pos = (Vec2<MapCoord>)map_pos;
        Vec2I idx_pos = (Vec2I)to_idx_pos(i);
        auto get_h = [&](Vec2I p) {
            return h_map[(p.x + 1) + (p.y + 1) * (CHK_SIZE + 2)];
        };
        F32 h = get_h(idx_pos);
        MapCoord f_h = ceil(h);

        Block block;
        if(map_pos.z > f_h) {
            block.id = void_block;
        } else {
            F32 diff =
                max(abs(get_h(idx_pos + Vec2I(1, 0)) - get_h(idx_pos - Vec2I(1, 0))),
                    abs(get_h(idx_pos + Vec2I(0, 1)) - get_h(idx_pos - Vec2I(0, 1))));
            diff *= 1.5f;
            if(map_pos.z >= f_h - 2.f) {
                if(diff < 1.7f) {
                    block.id = dark_grass;
                } else if(diff < 2.f) {
                    block.id = dirt;
                } else if(diff < 2.8f) {
                    block.id = raw_stone;
                } else {
                    block.id = stone_wall;
                }
            } else if(map_pos.z >= f_h - 4) {
                block.id = dirt;
            } else {
                block.id = raw_stone;
            }
        }
        get_block(i) = block;
    }
#endif
#if 0
    for(Uns i = 0; i < CHK_VOL; ++i) {
        MapPos map_pos = to_map_pos(pos, i);
        if(lux_randf(map_pos, 0) > 0.9999999f) {
            Vec3F dir = lux_rand_norm_3(map_pos, 1);
            Vec3F iter = (Vec3F)map_pos;
            for(Uns j = 0; j < 1000 * 3; ++j) {
                write_suspended_block((MapPos)floor(iter), {grass});
                iter[j % 3] += dir[j % 3];
            }
        }
    }
#endif
#if 1
    for(Uns i = 0; i < CHK_VOL; ++i) {
        MapCoord h = round(h_map[i & ((CHK_SIZE * CHK_SIZE) - 1)]);
        F32 f_h = ceil(h);
        MapPos map_pos = to_map_pos(pos, i);
        if(map_pos.z == f_h && lux_randf(map_pos) > .995f &&
            get_block(i).id == dark_grass) {
            Uns h = lux_randmm(8, 20, map_pos, 0);
            for(Uns j = 0; j < h; ++j) {
                write_suspended_block(map_pos + MapPos(0, 0, j), {dirt});
            }
            for(MapCoord z = -((Int)h / 2 + 2); z <= (Int)h / 2; ++z) {
                for(MapCoord y = -5; y <= 5; ++y) {
                    for(MapCoord x = -5; x <= 5; ++x) {
                        if((F32)((h / 2) + 2 - z) / 4.f > length(Vec2F(x, y))) {
                            write_suspended_block(map_pos + MapPos(x, y, h + z),
                                                  {grass});
                        }
                    }
                }
            }
        }
    }
#endif
#if 0
    MapPos base_pos = to_map_pos(pos, 0);
    if(pos.z < 0) {
        U32 worms_num = lux_randf(pos) > 0.99 ? 1 : 0;
        for(Uns i = 0; i < worms_num; ++i) {
            Vec3F dir;
            U32 len = lux_randmm(10, 500, pos, i, 0);
            dir = lux_rand_norm_3(pos, i, 1);
            Vec3F map_pos = to_map_pos(pos, lux_randm(CHK_VOL, pos, i, 2));
            F32 rad = lux_randfmm(2, 5, pos, i, 3);
            for(Uns j = 0; j < len; ++j) {
                rad = noise_fbm(((F32)(j + i) * 1000.f +
                    length((Vec3F)base_pos)) * 0.05f, 3);
                rad = u_norm(rad) * 9.f + 1.f;
                rad *= 1.f - abs(s_norm((F32)j / (F32)len));
                Vec2<ChkCoord> h_pos = to_chk_pos(map_pos);
                //@TODO we don't have that access rn
                HeightChunk const& h_chk = guarantee_height_chunk(h_pos);
                IdxPos idx_pos = to_idx_pos(map_pos);
                F32 h = h_chk[idx_pos.x + idx_pos.y * (CHK_SIZE + 1)];
                if(map_pos.z > h + rad) {
                    ///we have drilled too far into the surface by now
                    break;
                }
                MapCoord r_rad = ceil(rad);
                for(MapCoord z = -r_rad; z <= r_rad; ++z) {
                    for(MapCoord y = -r_rad; y <= r_rad; ++y) {
                        for(MapCoord x = -r_rad; x <= r_rad; ++x) {
                            if(length(Vec3F(x, y, z)) < rad) {
                                write_suspended_block(floor(map_pos + Vec3F(x, y, z)),
                                    {void_block});
                            }
                        }
                    }
                }
                map_pos += dir;
                Vec3F ch = {lux_randf(pos, i, 5, j, 0) - 0.5f,
                            lux_randf(pos, i, 5, j, 1) - 0.5f,
                            lux_randf(pos, i, 5, j, 2) - 0.5f};
                ch *= 0.4f;
                dir += ch;
                dir = normalize(dir);
            }
        }
    }
#endif
}

namespace chunk_disk_system {
    constexpr U32 region_sz_exp = 4;
    constexpr U32 sector_sz = 1 << 9;
    constexpr U32 table_length = 1 << (region_sz_exp * 3);

    using RegionPos   = Vec3<I32>;
    using InternalPos = Vec3<U8>;
    using InternalIdx = U16;
    static_assert(sizeof(InternalIdx) * 8 >= region_sz_exp * 3);

    static RegionPos get_region_pos(ChkPos pos) {
        return (RegionPos)pos >> (RegionPos::value_type)region_sz_exp;
    }
    static InternalIdx get_internal_idx(ChkPos pos) {
        InternalPos ip = (InternalPos)pos &
            (InternalPos::value_type)((1 << region_sz_exp) - 1);
        return ip.x | ((ip.y | (ip.z << region_sz_exp)) << region_sz_exp);
    }

    #pragma pack(push, 1)
    struct TableEntry {
        U8 sz;
        //@TODO bitset?
        U8 off[3];
        static constexpr U32 no_entry = static_max_val<decltype(off)>();
    };
    #pragma pack(pop)

    std::mutex files_mutex;
    struct RegionFile {
        //maybe we can use shared_mutex, but I'm not sure if it's a good idea
        //to let many threads read from one file
        std::mutex                    mutex;
        Arr<TableEntry, table_length> table;
        FILE* h;
    };
    constexpr U32 data_start_offset = sizeof(RegionFile::table);

    using Filename = Arr<char, 64>;
    static VecMap<RegionPos, SharingPtr<RegionFile>> files;
    void get_region_filename(Filename& out, RegionPos const& pos){
        LUX_ASSERT(snprintf(out, arr_len(out),
            "l.%d.%d.%d.region", pos.x, pos.y, pos.z) >= 0);
    }
    //if we exit with true, we hold the file's mutex, otherwise no lock held
    RegionFile* try_open_file(RegionPos const& pos) {
        std::lock_guard<std::mutex> lock(files_mutex);
        RegionFile* file;
        if(files.count(pos) > 0) {
            file = files.at(pos);
            file->mutex.lock();
            return file;
        } else {
            Filename filename_str;
            get_region_filename(filename_str, pos);
            FILE* f = fopen(filename_str, "r+b");
            if(f == nullptr) {
                return nullptr;
            }
            file = new RegionFile;
            file->h = f;
            file->mutex.lock();
            files[pos] = file;
            LUX_ASSERT(fread(&file->table, sizeof(file->table), 1, file->h)
                == 1);
            return file;
        }
    }
    //current thread needs to hold file's mutex
    //we exit with no locks held
    void try_close_file(RegionFile* file, RegionPos const& pos) {
        //@XXX for some reason sometimes not all files are closed
        //     usually one or two though, maybe the last ones?
        //
        //     this might also be harder to detect now, as the load/save is not
        //     that simple anymore, and threads have less chance of strucking
        //     the edgecases, this bug needs to be tested in isolated
        //     environments
        file->mutex.unlock();
        files_mutex.lock();
        if(files.count(pos) > 0) {
            file = files.at(pos);
            if(file->mutex.try_lock()) {
                files.erase(pos);
                file->mutex.unlock();
                LUX_ASSERT(fclose(file->h) == 0);
                delete file;
            }
        }
        files_mutex.unlock();
    }
}

static void load_chunk(Uninit<TaskOutput>& out, TaskInput& in) {
    using namespace chunk_disk_system;
    using namespace map;

    out.init(TaskOutput::LOAD_CHUNK);
    auto const& pos = in.load_chunk->pos;

    RegionPos region_pos = get_region_pos(pos);

    RegionFile* file = try_open_file(region_pos);
    if(file == nullptr) {
        generate_chunk(*out->load_chunk, *in.load_chunk);
        return;
    }

    InternalIdx internal_idx = get_internal_idx(pos);
    TableEntry table_entry = file->table[internal_idx];
    U32 off = 0;
    memcpy(&off, table_entry.off, sizeof(table_entry.off));
    if(off == TableEntry::no_entry) {
        try_close_file(file, region_pos);
        generate_chunk(*out->load_chunk, *in.load_chunk);
        return;
    }
    LUX_ASSERT(!fseek(file->h, data_start_offset + sector_sz * off, SEEK_SET));

    U32 byte_sz;
    LUX_ASSERT(fread(&byte_sz, sizeof(byte_sz), 1, file->h) == 1);

    //just a sanity test, the byte_sz can easily get corrupted and eat all the
    //RAM
    //@NOTE should be changed if the chunks' sizes can exceed 2MiB
    LUX_ASSERT(byte_sz < (1 << 20));
    DynArr<U8> in_buf(byte_sz);
    LUX_ASSERT(fread(in_buf.beg, byte_sz, 1, file->h) == 1);

    z_stream inf_strm;
    inf_strm.zalloc = Z_NULL;
    inf_strm.zfree  = Z_NULL;
    inf_strm.opaque = Z_NULL;
    inflateInit(&inf_strm);
    LUX_DEFER{inflateEnd(&inf_strm);};

    out->load_chunk->data = new Chunk::Data;

    inf_strm.avail_out = sizeof(Chunk::Data::blocks);
    inf_strm.next_out  = (Bytef*)out->load_chunk->data->blocks;
    inf_strm.avail_in  = byte_sz;
    inf_strm.next_in   = (Bytef*)in_buf.beg;
    LUX_ASSERT(inflate(&inf_strm, Z_FINISH) == Z_STREAM_END);
    LUX_ASSERT(inf_strm.avail_in == 0);

    try_close_file(file, region_pos);
}

static void unload_chunk(Uninit<TaskOutput>& out, TaskInput& in) {
    using namespace chunk_disk_system;
    using namespace map;

    out.init(TaskOutput::DISCARD);
    ChkPos       pos  = in.unload_chunk->pos;
    Chunk::Data* data = in.unload_chunk->data;

    //@TODO endianness?
    RegionPos region_pos = get_region_pos(pos);
    RegionFile* file = try_open_file(region_pos);
    if(file == nullptr) {
        //file doesn't exist so we create it
        files_mutex.lock();
        file = new RegionFile;
        file->mutex.lock();
        Filename filename_str;
        get_region_filename(filename_str, region_pos);
        file->h = fopen(filename_str, "wb");
        LUX_ASSERT(file->h != nullptr);
        LUX_ASSERT(fclose(file->h) == 0);
        file->h = fopen(filename_str, "r+b");
        LUX_ASSERT(file->h != nullptr);
        for(auto& entry : file->table) {
            entry.sz = 0;
            memcpy(entry.off, &TableEntry::no_entry, sizeof(entry.off));
        }
        LUX_ASSERT(fwrite(&file->table, sizeof(TableEntry), table_length,
            file->h) == table_length);
        files[region_pos] = file;
        files_mutex.unlock();
    }
    InternalIdx internal_idx = get_internal_idx(pos);
    U32 off = 0;
    memcpy(&off, file->table[internal_idx].off, sizeof(TableEntry::off));
    if(off == TableEntry::no_entry) {
        off = 0;
        for(auto const& entry : file->table) {
            U32 temp_off = 0;
            memcpy(&temp_off, entry.off, sizeof(entry.off));
            if(temp_off != TableEntry::no_entry) {
                LUX_ASSERT(entry.sz != 0);
                temp_off += entry.sz;
                if(temp_off > off) {
                    off = temp_off;
                }
            }
        }
        TableEntry new_entry;
        new_entry.sz = 0;
        memcpy(new_entry.off, &off, sizeof(new_entry.off));

        LUX_ASSERT(!fseek(file->h, sizeof(TableEntry) * internal_idx, SEEK_SET));
        LUX_ASSERT(fwrite(&new_entry, sizeof(TableEntry), 1, file->h) == 1);
        file->table[internal_idx] = new_entry;
    } else {
        LUX_ASSERT(file->table[internal_idx].sz != 0);
    }
    z_stream def_strm;
    def_strm.zalloc = Z_NULL;
    def_strm.zfree  = Z_NULL;
    def_strm.opaque = Z_NULL;
    deflateInit(&def_strm, Z_DEFAULT_COMPRESSION);
    LUX_DEFER{deflateEnd(&def_strm);};

    DynArr<Bytef> out_buf(deflateBound(&def_strm, sizeof(Chunk::Data::blocks)));

    def_strm.avail_in = sizeof(Chunk::Data::blocks);
    def_strm.next_in  = (Bytef*)in.unload_chunk->data->blocks;
    def_strm.avail_out = out_buf.len * sizeof(Bytef);
    def_strm.next_out  = out_buf.beg;
    LUX_ASSERT(deflate(&def_strm, Z_FINISH) == Z_STREAM_END);
    LUX_ASSERT(def_strm.avail_in == 0);
    U32 new_byte_sz = def_strm.total_out;

    U32 new_sz = (sizeof(U32) + new_byte_sz - 1) / sector_sz + 1;
    U32 old_sz = file->table[internal_idx].sz;
    LUX_ASSERT(new_sz != 0);
    if(new_sz != old_sz) {
        TableEntry new_entry;
        new_entry.sz = new_sz;
        memcpy(new_entry.off, &off, sizeof(new_entry.off));

        LUX_ASSERT(!fseek(file->h, sizeof(TableEntry) * internal_idx, SEEK_SET));
        LUX_ASSERT(fwrite(&new_entry, sizeof(TableEntry), 1, file->h) == 1);
        file->table[internal_idx] = new_entry;
        if(old_sz > 0) {
            //we shift sectors
            LUX_ASSERT(!fseek(file->h, 0, SEEK_END));
            SizeT file_sz = ftell(file->h);
            LUX_ASSERT(file_sz != EOF);
            SizeT old_end = data_start_offset + (off + old_sz) * sector_sz;
            SizeT new_end = data_start_offset + (off + new_sz) * sector_sz;
            LUX_ASSERT(file_sz > old_end);
            SizeT shift_sz = file_sz - old_end;
            DynArr<U8> buf(shift_sz);
            LUX_ASSERT(!fseek(file->h, old_end, SEEK_SET));
            LUX_ASSERT(fread(buf.beg, shift_sz, 1, file->h) == 1);
            LUX_ASSERT(!fseek(file->h, new_end, SEEK_SET));
            LUX_ASSERT(fwrite(buf.beg, shift_sz, 1, file->h) == 1);

            //now we need to update other chunk's offsets in the table
            I32 diff = (I32)new_sz - (I32)old_sz;
            for(Uns i = 0; i < table_length; ++i) {
                U32 old_off = 0;
                memcpy(&old_off, file->table[i].off, sizeof(TableEntry::off));
                if(old_off != TableEntry::no_entry && old_off > off) {
                    U32 new_off = (I32)old_off + diff;
                    memcpy(file->table[i].off, &new_off,
                           sizeof(TableEntry::off));
                }
            }
            LUX_ASSERT(!fseek(file->h, 0, SEEK_SET));
            LUX_ASSERT(fwrite(&file->table, sizeof(TableEntry) * table_length,
                1, file->h) == 1);
        }
    }
    LUX_ASSERT(!fseek(file->h, data_start_offset + off * sector_sz, SEEK_SET));
    LUX_ASSERT(fwrite(&new_byte_sz, sizeof(U32), 1, file->h) == 1);
    LUX_ASSERT(fwrite(out_buf.beg, new_byte_sz, 1, file->h) == 1);
    try_close_file(file, region_pos);

    delete data;
}

static void build_chunk_mesh(Uninit<TaskOutput>& out, TaskInput& in) {
    using namespace map;

    out.init(TaskOutput::BUILD_CHUNK_MESH);
    ChkPos pos       = in.build_chunk_mesh->pos;
    auto const& data = in.build_chunk_mesh->data;

    auto& faces = out->build_chunk_mesh->faces;
    faces.reserve_exactly(CHK_VOL * 3);

    auto get_block_l = [&](Vec3U pos) -> Block {
        Uns ci = 0;
        for(Uns a = 0; a < 3; ++a) {
            if(pos[a] == CHK_SIZE) {
                ci = a + 1;
                pos[a] = 0;
                break;
            }
        }
        ChkIdx idx = to_chk_idx(IdxPos(pos));
        return data[ci]->read(idx);
    };

    Arr<IdxPos, 3> a_off = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    for(Uns z = 0; z < CHK_SIZE; ++z) {
        for(Uns y = 0; y < CHK_SIZE; ++y) {
            for(Uns x = 0; x < CHK_SIZE; ++x) {
                for(Uns a = 0; a < 3; ++a) {
                    IdxPos idx_pos(x, y, z);
                    Block b0 = get_block_l(idx_pos);
                    Block b1 = get_block_l(idx_pos + a_off[a]);
                    if((b0.id == void_block) != (b1.id == void_block)) {
                        U8 orient = b0.id != void_block;
                        BlockId id = orient ? b0.id : b1.id;
                        faces.push({to_chk_idx(idx_pos), id,
                            (a << 1 | orient)});
                    }
                }
            }
        }
    }
    faces.shrink_to_fit();
}

static TaskId get_next_task_id() {
    //this might run out after 100 years of running or so, idk
    static TaskId id_counter = 0;
    return id_counter++;
}

define(`m4_task_input_types', `
    $1(`generate_height_map', `shift($@)')
    $1(`load_chunk', `shift($@)')
    $1(`unload_chunk', `shift($@)')
    $1(`build_chunk_mesh', `shift($@)')
')

define(`m4_task_output_types', `
    $1(`discard', `shift($@)')
    $1(`generate_height_map', `shift($@)')
    $1(`load_chunk', `shift($@)')
    $1(`build_chunk_mesh', `shift($@)')
')

define(`m4_to_upper', `translit(`$1', `a-z', `A-Z')')
define(`m4_to_typename',
    `patsubst(patsubst(patsubst(`$1',`_\([a-z]\)',` m4_to_upper(`\1')'),` '),`^\([a-z]\)', `m4_to_upper(`\1')')')

define(`m4_task_input_ctor', `
    template<>
    TaskInput::TaskInput<TaskInput::m4_to_typename(`$1')>(TaskInput::m4_to_typename(`$1')&& in) {
        type = m4_to_upper(`$1');
        $1.init(move(in));
        id = get_next_task_id();
    }
')

m4_task_input_types(`m4_task_input_ctor');

define(`m4_case', `
    case TaskInput::m4_to_upper(`$1'): {
        $1(out, in);
        break;
    }
')

void do_task(Uninit<TaskOutput>& out, TaskInput& in) {
    switch(in.type) {
    //@TODO macros?
        m4_task_input_types(`m4_case')
        default: LUX_UNREACHABLE();
    }
}

define(`m4_case', `
    case m4_to_upper(`$1'): {
        patsubst(`$2', `%v', $1)
        break;
    }
')

define(`m4_task_function', `
    $1 {
        $2
        switch(type) {
            m4_task_$3_types(`m4_case', `$4')
            default: LUX_UNREACHABLE();
        }
        $5
    }
')

m4_task_function(`TaskInput::~TaskInput()', , `input', `%v.deinit();',)
m4_task_function(`TaskInput::TaskInput(TaskInput&& that)',`
    type = that.type;
    id   = that.id;
    ', `input', `%v.init(move(*that.%v));',)
m4_task_function(`TaskInput& TaskInput::operator=(TaskInput&& that)',`
    type = that.type;
    id   = that.id;
    ', `input', `*%v = move(*that.%v);',
    `return *this;')
m4_task_function(`TaskOutput::TaskOutput(TaskOutput::Type const& _type)',`
    type = _type;
    '
    , `output', `%v.init();',)
m4_task_function(`TaskOutput::~TaskOutput()', , `output', `%v.deinit();',)
m4_task_function(`TaskOutput::TaskOutput(TaskOutput&& that)',`
    type = that.type;
    '
    , `output', `%v.init(move(*that.%v));',)
m4_task_function(`TaskOutput& TaskOutput::operator=(TaskOutput&& that)',`
    type = that.type;
    '
    , `output', `*%v = move(*that.%v);',
    `return *this;')

}
