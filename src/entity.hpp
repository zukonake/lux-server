#pragma once

#include <lux_shared/common.hpp>
#include <lux_shared/entity.hpp>
#include <lux_shared/net/data.hpp>
//
#include <physics.hpp>
#include <tick_time_data.hpp>

/* currenty we use an entity component system
 * both client and server maintain their own specific sets of components for
 * each entity
 * there is also the intermediate network format for entity components
 */

namespace entity {

using ComponentId = U16;

template<typename T>
bool try_get_controller(Uninit<T>& out, EntityId);

struct PlayerController {
    ComponentId body_id;
    ComponentId terrestial_id;
    ComponentId inventory_id;
    ComponentId must_eat_id;

    void move(Vec2F const& dir);
    void jump();
    void set_yaw_pitch(F32 yaw, F32 pitch);
    void break_block();
    void place_block();
    void eat(EntityId id);
};

EntityId create_player();
void erase(EntityId entity);
void tick(TickTimeData const&);
void get_net_entities(NetSsTick::Entities& out);
void deinit();

}
