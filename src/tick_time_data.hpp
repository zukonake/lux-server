#pragma once

#include <lux_shared/int.hpp>

//@TODO move to shared?
struct TickTimeData {
    F32 day_cycle;
    U64 tick_num;
    U16 tick_rate;
    F64 delta;
};
