#pragma once

#include <bullet/btBulletDynamicsCommon.h>
//
#include <lux_shared/common.hpp>
#include <lux_shared/entity.hpp>
#include <lux_shared/map.hpp>

namespace physics {

void init();
void deinit();
OwningPtr<btRigidBody> create_body(EntityVec const& pos);
OwningPtr<btRigidBody> create_mesh(MapPos const& pos,
                                             btCollisionShape* shape);
void remove_body(OwningPtr<btRigidBody> body);
void tick(F64 time);
void ray_test(btCollisionWorld::RayResultCallback& out,
                      Vec3F const& from, Vec3F const& to);
}
