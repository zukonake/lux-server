#include <physics.hpp>

namespace physics {

static btCapsuleShapeZ body_shape(0.8f, 2.f);

static btDbvtBroadphase                    broadphase;
static btDefaultCollisionConfiguration     collision_conf;
static btCollisionDispatcher               dispatcher(&collision_conf);
static btSequentialImpulseConstraintSolver solver;
static btDiscreteDynamicsWorld             world(&dispatcher, &broadphase,
                                                 &solver, &collision_conf);

void init() {
    world.setGravity(btVector3(0, 0, -9.8 * 2.f));
}

void deinit() {
    LUX_ASSERT(world.getCollisionObjectArray().size() == 0);
}

OwningPtr<btRigidBody> create_body(EntityVec const& pos) {
    btDefaultMotionState* ms = new btDefaultMotionState(
        btTransform({0, 0, 0, 1}, {pos.x, pos.y, pos.z}));
    F32 mass = 60.f;
    btVector3 inertia;
    body_shape.calculateLocalInertia(mass, inertia);
    btRigidBody::btRigidBodyConstructionInfo ci(mass, ms, &body_shape, inertia);
    ci.m_friction = 1.f;
    btRigidBody* body = new btRigidBody(ci);
    body->forceActivationState(DISABLE_DEACTIVATION);
    body->setDamping(0.7f, 0.8f);
    world.addRigidBody(body);
    return body;
}

OwningPtr<btRigidBody> create_mesh(MapPos const& pos,
                                            btCollisionShape* shape) {
    btDefaultMotionState* ms = new btDefaultMotionState(
        btTransform({0, 0, 0, 1}, {pos.x, pos.y, pos.z}));
    btRigidBody::btRigidBodyConstructionInfo ci(0, ms, shape,
        btVector3(0, 0, 0));
    btRigidBody* body = new btRigidBody(ci);
    world.addRigidBody(body);
    return body;
}

void remove_body(OwningPtr<btRigidBody> body) {
    auto* ms = body->getMotionState();
    if(ms != nullptr) delete ms;
    world.removeRigidBody(body);
    delete body;
}

void tick(F64 time) {
    constexpr F64 physics_step_time = 1.0 / 64.0;
    //world.stepSimulation(time, (time / physics_step_time) + 1, physics_step_time);
    //@TODO for now we do that, because debugging is impossible as currently the
    //player falls of the map due to physics meshes not generating fast enough
    world.stepSimulation(physics_step_time, 1, physics_step_time);
}

void ray_test(btCollisionWorld::RayResultCallback& out,
                      Vec3F const& from, Vec3F const& to) {
    btVector3 bt_from(from.x, from.y, from.z);
    btVector3 bt_to(to.x, to.y, to.z);

    world.rayTest(bt_from, bt_to, out);
}

}
